export class Traduccion {
  textoTraducir: string;
  textoTraducido: string;
  idioma: string;
  idiomaSecundario: string;
  idTraduccion : number;
}
