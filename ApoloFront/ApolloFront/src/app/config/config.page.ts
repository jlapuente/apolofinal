import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { makeDecorator } from '@angular/core/src/util/decorators';
import { componentFactoryName } from '@angular/compiler';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-config',
  templateUrl: './config.page.html',
  styleUrls: ['./config.page.scss']
})
export class ConfigPage implements OnInit {
  optUsername: string;
  contrast = 0;
  username: string;
  textSize: any;
  blindMode: any;
  debug: string;
  constructor(
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private prefs: AppPreferences
  ) {}

  ngOnInit() {
    this.storage.get('textSize').then(res => {
      this.textSize = res;
      this.changeSize();
    });
  }

  guardarConfiguracion() {
    this.storage.set('textSize', this.textSize);
    this.storage.set('blindMode', this.blindMode);
    this.showMessage('Configuracion guardada correctamente', 3000);
    this.changeSize();
    this.storage.set('tutorial', 'false');
  }
  async showMessage(mensaje: string, duracion: number) {
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: duracion,
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: 'ok'
    });
    toast.present();
  }
  changeSize() {
    const htmlRoot: HTMLElement = <HTMLElement>(
      document.getElementsByTagName('html')[0]
    );
    if (htmlRoot != null) {
      htmlRoot.style.fontSize = this.textSize + 'rem';
    }
    const root: HTMLElement = <HTMLElement>(
      document.getElementsByTagName('root')[0]
    );
    if (root != null) {
      root.style.fontSize = this.textSize + 'rem';
    }
  }
}
