import { Injectable } from '@angular/core';
import { Traduccion } from '../classes/traduccion';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TranslatorService {
  
  constructor(private http: HttpClient) { }

  translate(traduccion : Traduccion){
    const headers = new HttpHeaders();
   
    let params : HttpParams;

    params.append("source",traduccion.idioma);
    params.append("target",traduccion.idiomaSecundario);
    params.append("q",traduccion.textoTraducir);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      params : params
    };
    this.http.post<any[]>('https://translation.googleapis.com/language/translate/v2?&key=AIzaSyDINWpPIz9BQ__Pb-m3uw4F4HrMUq6lEaI', httpOptions).subscribe(
      data => {
        console.log('POST Request is successful ', data);
      },
      error => {
        console.log('Error', error);
      }
    );
  }
}
