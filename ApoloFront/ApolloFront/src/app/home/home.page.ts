import { Traduccion } from './../classes/traduccion';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { NavController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { TranslatorService } from '../services/translator.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureAudioOptions } from '@ionic-native/media-capture/ngx';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {

  textoTraducir: string;
  textoTraducido: string;
  idiomaPrimario: string;
  idiomaPrimarioTemp: string;
  idiomaSecundario: string;
  idiomaSecundarioTemp: string;
  timeLeft: number;
  interval;
  isFav: boolean;
  temp;

  langList: { value: string, text: string }[] = [
    { "value": "en", "text": "English" },
    { "value": "es", "text": "Spanish" },
    { "value": "fr", "text": "French" },
    { "value": "pt", "text": "Portuguese" },
    { "value": "cs", "text": "Czech" },
    { "value": "no", "text": "Norwegian" },
    { "value": "de", "text": "German" },
    { "value": "eo", "text": "Esperanto" },
    { "value": "ro", "text": "Romanian" },
    { "value": "it", "text": "Italian" }
  ];

  constructor(
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private prefs: AppPreferences,
    private nativeAudio: NativeAudio,
    private translatorService: TranslatorService,
    private http: HttpClient,
    private mediaCapture: MediaCapture
  ) { }
  // Config Variables
  textSize: number;

  ngOnInit() {
    this.textoTraducido = '';
    this.textoTraducir = '';
    this.idiomaPrimario = 'es';
    this.idiomaPrimarioTemp = 'es';
    this.idiomaSecundario = 'en';
    this.idiomaSecundarioTemp = 'en';
    this.isFav = false;
    //this.empezarTutorial();
  }
  keyUp() {
    // No haria falta en principio...
  }
  keyDown() {
    if (this.temp) {
      clearTimeout(this.temp);
    }
    this.temp = setTimeout(() => {
      this.translate();
    }, 1500);
    console.log(this.temp);
    /*  this.interval = setInterval(() => {
       if (this.timeLeft > 0) {
         this.timeLeft--;
       } else {
         // Llamamos al servicio y limpiamos intervalo
         this.translate();
         clearInterval(this.interval);
       }
     }, 1000); */
    if (this.isFav) {
      this.isFav = false;
    }
  }

  compareFn(e1: number, e2: number): boolean {
    return e1 && e2 ? e1 === e2 : e1 === e2;
  }
  addFav() {
    if (!this.isFav) {
      const traduccionFav = new Traduccion();
      traduccionFav.textoTraducir = this.textoTraducir;
      traduccionFav.idiomaSecundario = this.idiomaSecundario;
      traduccionFav.idioma = this.idiomaPrimario;
      traduccionFav.textoTraducido = this.textoTraducido;
      this.storage.get('favList').then(data => {
        let favList: Traduccion[];
        favList = [];
        if (data != null) {
          let jsonFavList = data;
          // Obtenemos la lista y la pasamos a objeto
          favList = JSON.parse(jsonFavList);
          let trad = favList[favList.length - 1];
          let id;
          if (trad.idTraduccion == null || trad.idTraduccion == undefined) {
            id = 1;
          } else {
            id = trad.idTraduccion + 1;

          }
          traduccionFav.idTraduccion = id;
          favList.push(traduccionFav);
          jsonFavList = JSON.stringify(favList);

          this.storage.set('favList', jsonFavList);
        } else {
          traduccionFav.idTraduccion = 1;
          favList.push(traduccionFav);
          let jsonFavList = JSON.stringify(favList);
          this.storage.set('favList', jsonFavList);
        }
      });
      this.isFav = true;
    } else {
      this.isFav = false;
    }
  }
  empezarTutorial() {
    //this.platform.ready().then(() => {
    this.nativeAudio.preloadComplex('uniqueId1', 'assets/sound/audio1.mp3', 100, 1, 0).then(
      function (success) {
        console.log(success);
        // can optionally pass a callback to be called when the file is done playing
        this.nativeAudio.play('uniqueId1', () => console.log('uniqueId1 is done playing'));
      }, function (err) {
        console.log(err);
        console.log('aaa');
      });
    //});
  }

  reproducirSonido1() {

  }
  micRecord() {
    let options: CaptureAudioOptions = { limit: 1 }
    this.mediaCapture.captureAudio(options)
      .then(
        (data: MediaFile[]) => console.log(data),
        (err: CaptureError) => console.error(err)
      );
  }
  translate() {
    let traduccion: Traduccion;
    traduccion = new Traduccion;
    traduccion.idioma = this.idiomaPrimario;
    traduccion.idiomaSecundario = this.idiomaSecundario;
    traduccion.textoTraducir = this.textoTraducir;
    const headers = new HttpHeaders();
    console.warn("he entrado");
    let parametros: HttpParams;
    parametros = new HttpParams().append("source", traduccion.idioma).append("target", traduccion.idiomaSecundario).append("q", traduccion.textoTraducir);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      params: parametros
    };
    //(( new HttpParams().set('ACTION','TRADUCIR.Traductor').set("source",traduccion.idioma)
    // .set("target",traduccion.idiomaSecundario).set("q",traduccion.textoTraducir)))
    this.http.get<any[]>('https://translation.googleapis.com/language/translate/v2?&key=AIzaSyDINWpPIz9BQ__Pb-m3uw4F4HrMUq6lEaI', httpOptions).subscribe(
      data => {
        console.log('POST Request is successful ', data);
        let JSONRecibido: any;
        JSONRecibido = data;
        this.textoTraducido = JSONRecibido.data.translations[0].translatedText;
      },
      error => {
        console.log('Error', error);
      }
    );
  }
  cambioSelect(event) {
    let id: string;
    id = event.target.id;
    console.log(id);
    console.log(event);
    if (this.idiomaSecundario == this.idiomaPrimario) {
      if (id == 'primarySelect') {
        this.idiomaSecundario = this.idiomaPrimarioTemp;
      } else {
        this.idiomaPrimario = this.idiomaSecundarioTemp;
      }
    }
    this.idiomaPrimarioTemp = this.idiomaPrimario;
    this.idiomaSecundarioTemp = this.idiomaSecundario;
  }
}

