import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { HttpClientModule } from '@angular/common/http'
import { MediaCapture } from '@ionic-native/media-capture/ngx';
@NgModule({
   declarations: [
      AppComponent,
   ],
   entryComponents: [],
   imports: [
      BrowserModule,
      HttpClientModule,
      IonicModule.forRoot(),
      AppRoutingModule,
      IonicStorageModule.forRoot()
   ],
   providers: [
      StatusBar,
      MediaCapture,
      AppPreferences,
      NativeAudio,
      SplashScreen,
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
