import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'Config',
      url: '/config',
      icon: 'settings'
    },
    {
      title: 'About Us',
      url: '/about-us',
      icon: 'person'
    },
    {
      title: 'Chat Individual',
      url: '/chat',
      icon: 'person'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private prefs: AppPreferences
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      /*let cond;
      // cond = this.prefs.fetch('tutorial');
      if ((cond = 'true')) {
        // if not first time
        this.router.navigateByUrl('/');
      } else {
        this.router.navigateByUrl('/config');
      }
      */
    });
  }
}
