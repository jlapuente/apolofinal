import { Traduccion } from './../classes/traduccion';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  private selectedItem: any;
  colorList: boolean;
  favList: Traduccion[];
  isList: boolean;
  selectedTraduccion: Traduccion;
  public items: Array<{ title: string; note: string; icon: string }> = [];
  constructor(private storage: Storage) {
    this.colorList = true;
    this.storage.get('favList').then(data => {
      const jsonFavList = data;
      console.log(data);
      if(data != null){
        this.favList = JSON.parse(jsonFavList);
        for (const fav of this.favList) {
          if (fav.textoTraducido.length >= 30) {
            fav.textoTraducido = fav.textoTraducido.substring(0, 25);
            fav.textoTraducido = fav.textoTraducido + '...';
            fav.textoTraducir = fav.textoTraducir.substring(0, 25);
            fav.textoTraducir = fav.textoTraducir + '...';
          }
        }
      } else {
        this.favList = [];
      }
      
      this.isList = true;
    });
  }

  ngOnInit() { }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }

  changeColor() {
    this.colorList = !this.colorList;
  }
  reiniciarDB() {
    this.storage.remove('favList');
  }
  detalle(id: number) {
    console.log(id);
    for (const fav of this.favList) {
      if (fav.idTraduccion == id) {
        this.selectedTraduccion = fav;
        console.log(this.selectedTraduccion);
        this.isList = !this.isList;
      }
    }
    
  }
}
