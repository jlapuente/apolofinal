
package com.mycompany.traductorapolo.beans.speech_text;

public class Config {
    private String encoding;
    private String languageCode;

    public Config() {

    }

    public Config(String encoding, String languageCode) {
        this.encoding = encoding;
        this.languageCode = languageCode;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public String toString() {
        return "Config{" + "encoding=" + encoding + ", languageCode=" + languageCode + '}';
    }

}
