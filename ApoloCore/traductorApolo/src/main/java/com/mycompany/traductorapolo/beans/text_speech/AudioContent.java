package com.mycompany.traductorapolo.beans.text_speech;

public class AudioContent {
    private String audioContent;

    public AudioContent() {
        
    }

    public AudioContent(String audioContent) {
        this.audioContent = audioContent;
    }

    public String getAudioContent() {
        return audioContent;
    }

    public void setAudioContent(String audioContent) {
        this.audioContent = audioContent;
    }

    @Override
    public String toString() {
        return "AudioContent{" + "audioContent=" + audioContent + '}';
    }
}
