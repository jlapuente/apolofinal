
package com.mycompany.traductorapolo.beans.speech_text;

public class Audio {
    private String content;
    private String uri;

    public Audio() {
        
    }

    public Audio(String content, String uri) {
        this.content = content;
        this.uri = uri;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "Audio{" + "content=" + content + ", uri=" + uri + '}';
    }
 
}
