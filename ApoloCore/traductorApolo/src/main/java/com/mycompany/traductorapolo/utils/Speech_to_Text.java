package com.mycompany.traductorapolo.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class Speech_to_Text {

    public String sendPost(String json) throws IOException {

        String urlFormada = "https://speech.googleapis.com/v1/speech:recognize?key=AIzaSyBZMZwv1-WLSMG-OBJiaDQ6Q9oYEff5vEo";

        URL url = new URL(urlFormada);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(con.getOutputStream());

        wr.writeBytes(json);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        String contenido = response.toString();

        //Sacamos el campo "transcript"
//        JsonElement jelement = new JsonParser().parse(contenido);
//        JsonObject jobject = jelement.getAsJsonObject();
//
//        JsonArray jarray = jobject.getAsJsonArray("results");
//        String clave = "";
//
//        for (int i = 0; i < jarray.size(); i++) {
//            JsonObject object = (JsonObject) jarray.get(i);
//            clave = object.get("alternatives").toString();
//
//        }
//
//        System.out.println("");
//        System.out.println(".......TRANSCRIPCION.......");
//        System.out.println(clave);
//
//        System.out.println("");
        
        return contenido;
    }
}
