package com.mycompany.traductorapolo.beans.text_speech;

public class AudioConfig {
    private String audioEncoding;

    public AudioConfig(){
        
    }
    public AudioConfig(String audioEncoding) {
        this.audioEncoding = audioEncoding;
    }

    public String getAudioEncoding() {
        return audioEncoding;
    }

    public void setAudioEncoding(String audioEncoding) {
        this.audioEncoding = audioEncoding;
    }

    @Override
    public String toString() {
        return "AudioConfig{" + "audioEncoding=" + audioEncoding + '}';
    }

}
