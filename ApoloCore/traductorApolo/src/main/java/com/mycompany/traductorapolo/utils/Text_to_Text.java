package com.mycompany.traductorapolo.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mycompany.traductorapolo.beans.translator.Traductor;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Base64;
import javax.net.ssl.HttpsURLConnection;

public class Text_to_Text {

    public String sendPost(Traductor traductor) throws Exception {
        String source = traductor.getSource();
        String target = traductor.getTarget();
        String q = traductor.getQ();
        System.out.println(q);
        System.out.println(source);
        System.out.println(target);
        String urlFormada = "https://translation.googleapis.com/language/translate/v2?&key=AIzaSyDINWpPIz9BQ__Pb-m3uw4F4HrMUq6lEaI&source=" + source + "&target=" + target + "&q=" + URLEncoder.encode(q, "UTF-8");

        URL url = new URL(urlFormada);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

        con.setRequestMethod("POST");

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());

        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
            System.out.println(inputLine);
        }
        in.close();

        String contenido = response.toString();
        System.out.println(contenido);
        //Sacamos el campo "translatedText"
//        JsonElement jelement = new JsonParser().parse(contenido);
//        JsonObject jobject = jelement.getAsJsonObject();
//        jobject = jobject.getAsJsonObject("data");
//        JsonArray jarray = jobject.getAsJsonArray("translations");
//        jobject = jarray.get(0).getAsJsonObject();
//        String result = jobject.get("translatedText").getAsString();

        return contenido;
    }
}
