
package com.mycompany.traductorapolo.beans.text_speech;

public class Text_Speech {
   
   private Input input;
   private Voice voice;
   private AudioConfig audioConfig;

    public Text_Speech(){
        
    }
    public Text_Speech(Input input, Voice voice, AudioConfig audioConfig) {
        this.input = input;
        this.voice = voice;
        this.audioConfig = audioConfig;
    }

    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }

    public Voice getVoice() {
        return voice;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    public AudioConfig getAudioConfig() {
        return audioConfig;
    }

    public void setAudioConfig(AudioConfig audioConfig) {
        this.audioConfig = audioConfig;
    }

    @Override
    public String toString() {
        return "Text_Speech{" + "input=" + input + ", voice=" + voice + ", audioConfig=" + audioConfig + '}';
    }
  
}
