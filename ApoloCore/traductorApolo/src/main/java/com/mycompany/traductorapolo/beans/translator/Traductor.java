
package com.mycompany.traductorapolo.beans.translator;


public class Traductor {

    private String source;
    private String target;
    private String q;

    public Traductor() {

    }

    public Traductor(String source, String target, String q) {
        this.source = source;
        this.target = target;
        this.q = q;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    @Override
    public String toString() {
        return "Traductor{" + "source=" + source + ", target=" + target + ", q=" + q + '}';
    }
    
}
