package com.mycompany.traductorapolo.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mycompany.traductorapolo.beans.text_speech.AudioContent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.apache.commons.codec.binary.Base64;

public class Text_to_Speech {

    public String sendPost(String json) throws Exception {
        String urlFormada = "https://texttospeech.googleapis.com/v1/text:synthesize?key=AIzaSyAx7OJxg3VudzzqoN9564urgcAv7Sv0rX0";

        URL url = new URL(urlFormada);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(con.getOutputStream());

        wr.writeBytes(json);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        String contenido = response.toString();

        //SACAR "audioContent"
        JsonElement jelement = new JsonParser().parse(contenido);
        JsonObject jobject = jelement.getAsJsonObject();
        String result = jobject.get("audioContent").getAsString();

        //DESCODIFICAR
        byte[] byteArray = Base64.decodeBase64(result.getBytes("UTF-8"));
        String decodedString = new String(byteArray);
              
        AudioContent audioContent = new AudioContent();
        audioContent.setAudioContent(decodedString);
        
        Gson gson = new Gson();
        String resultado = gson.toJson(audioContent);     
        
       //GENERAR FICHERO
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("pruebaBase64.mp3")))) {
            writer.write(decodedString);
        }

        return resultado;
    }
}
