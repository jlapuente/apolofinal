
package com.mycompany.traductorapolo.beans.speech_text;

public class Speech_Text {
    private Config config;
    private Audio audio;

    public Speech_Text(){
        
    }
    public Speech_Text(Config config, Audio audio) {
        this.config = config;
        this.audio = audio;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    @Override
    public String toString() {
        return "Speech_Text{" + "config=" + config + ", audio=" + audio + '}';
    } 
    
}
