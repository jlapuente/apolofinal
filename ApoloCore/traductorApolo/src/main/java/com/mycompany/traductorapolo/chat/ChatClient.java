package com.mycompany.traductorapolo.chat;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


class ChatClient {

    public static int port = 8080;
    public static BufferedReader con_br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        Socket sock = new Socket("192.168.1.38", port);
        BufferedReader sock_br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        PrintWriter sock_pw = new PrintWriter(sock.getOutputStream(), true);
        System.out.println("Conexión establecida");

        Thread chat_client_writer = new ChatWriter("chat_client_writer", sock_pw, con_br);
        chat_client_writer.start();

        String s;
        while ((s = sock_br.readLine()) != null) {
            System.out.println("\rserver: " + s);
            System.out.print("> ");
        }
        sock.close();
    }
}
