package com.mycompany.traductorapolo.controller;

import com.google.gson.Gson;
import com.mycompany.traductorapolo.beans.speech_text.Audio;
import com.mycompany.traductorapolo.utils.Speech_to_Text;
import com.mycompany.traductorapolo.utils.Text_to_Text;
import com.mycompany.traductorapolo.utils.Text_to_Speech;
import com.mycompany.traductorapolo.beans.text_speech.AudioConfig;
import com.mycompany.traductorapolo.beans.speech_text.Config;
import com.mycompany.traductorapolo.beans.text_speech.Input;
import com.mycompany.traductorapolo.beans.speech_text.Speech_Text;
import com.mycompany.traductorapolo.beans.text_speech.Text_Speech;
import com.mycompany.traductorapolo.beans.translator.Traductor;
import com.mycompany.traductorapolo.beans.text_speech.Voice;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;

public class TraducirAction {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String pagDestino = "";
        String action = (String) request.getParameter("ACTION");
        String[] arrayAction = action.split("\\.");
        System.out.println(arrayAction[1]);
        switch (arrayAction[1]) {
            case "Traductor":
                pagDestino = traductor(request, response);
                break;
            case "Speech_to_Text":
                pagDestino = speech_Text(request, response);
                break;
            case "Text_to_Speech":
                pagDestino = text_Speech(request, response);
                break;
        }
        return pagDestino;
    }

    public String traductor(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String source = (String) request.getParameter("source");
        String target = (String) request.getParameter("target");
        String q = (String) request.getParameter("q");

        Traductor traductor = new Traductor();
        traductor.setSource(source);
        traductor.setTarget(target);
        traductor.setQ(q);

        Text_to_Text text_to_Text = new Text_to_Text();
        String contenido = text_to_Text.sendPost(traductor);
        System.out.println(contenido);
        return contenido;
    }
    public String speech_Text(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //Extraemos el fichero de audio
        File file = new File("C:\\Users\\pablo\\Desktop\\traductorApolo\\audio español para ver si se escucha bien.mp3");
        byte[] bytes = cargarFichero(file);
        byte[] encoded = Base64.encodeBase64(bytes);

        String languageCode = (String) request.getParameter("languageCode");
        String content = new String(encoded);

        //Body json
        Gson gson = new Gson();
        Speech_Text speech_Text = new Speech_Text();
        Config config = new Config();
        config.setEncoding("LINEAR16");
        config.setLanguageCode(languageCode);

        Audio audio = new Audio();
        audio.setContent(content);//String en Base64

        speech_Text.setConfig(config);
        speech_Text.setAudio(audio);

        String json = gson.toJson(speech_Text);

        Speech_to_Text speech_to_Text = new Speech_to_Text();
        String contenido = speech_to_Text.sendPost(json);

        return contenido;
    }
    private String text_Speech(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String text = (String) request.getParameter("text");
        String languageCode = (String) request.getParameter("languageCode");
        String name = (String) request.getParameter("name");

        //Body json
        Gson gson = new Gson();
        Text_Speech text_Speech = new Text_Speech();
        Input input = new Input();
        input.setText(text);

        Voice voice = new Voice();
        voice.setLanguageCode(languageCode);
        voice.setName(name);
        voice.setSsmlGender("FEMALE");

        AudioConfig audioConfig = new AudioConfig();
        audioConfig.setAudioEncoding("LINEAR16");

        text_Speech.setInput(input);
        text_Speech.setVoice(voice);
        text_Speech.setAudioConfig(audioConfig);

        String json = gson.toJson(text_Speech);

        Text_to_Speech text_to_Speech = new Text_to_Speech();
        String contenido = text_to_Speech.sendPost(json);

        return contenido;
    }

    public static byte[] cargarFichero(File file) throws IOException {
            InputStream is = new FileInputStream(file);

            long length = file.length();

            //Sacamos longitud
            byte[] bytes = new byte[(int) length];

            int offset = 0;
            int numRead = 0;
                while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                    offset += numRead;
                }

            //Si no hay nada escrito...
            if (offset < bytes.length) {
                throw new IOException("¡Escribe algo!. No tienes ningún carácter escrito" + file.getName());
            }

            is.close();
            
        return bytes;
    }
}
