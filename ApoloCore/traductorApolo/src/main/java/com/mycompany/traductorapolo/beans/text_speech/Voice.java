package com.mycompany.traductorapolo.beans.text_speech;

public class Voice {
    private String languageCode;
    private String name;
    private String ssmlGender;

    public Voice(){
        
    }
    public Voice(String languageCode, String name, String ssmlGender) {
        this.languageCode = languageCode;
        this.name = name;
        this.ssmlGender = ssmlGender;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSsmlGender() {
        return ssmlGender;
    }

    public void setSsmlGender(String ssmlGender) {
        this.ssmlGender = ssmlGender;
    }
    
    
}
