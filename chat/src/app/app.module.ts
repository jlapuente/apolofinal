import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomePage } from './home/home.page';
import { ChatPage } from './chat/chat.page';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { HttpClientModule } from '@angular/common/http';


var firebaseConfig = {
  apiKey: "AIzaSyBDZfyEE3DTLmWSa6U_AGo71ed1Qu-fEmE",
  authDomain: "apolo-chat-a85a6.firebaseapp.com",
  databaseURL: "https://apolo-chat-a85a6.firebaseio.com",
  projectId: "apolo-chat-a85a6",
  storageBucket: "apolo-chat-a85a6.appspot.com",
  messagingSenderId: "795892515478",
  appId: "1:795892515478:web:8007d143ee2161fe"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(),HttpClientModule, AppRoutingModule, AngularFireModule.initializeApp(firebaseConfig), AngularFireDatabaseModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
