import { Component, OnInit, ViewChild, ContentChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireDatabase } from '@angular/fire/database';
import { UpperCasePipe } from '@angular/common';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { utf8Encode } from '@angular/compiler/src/util';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  username : string;
  message : string;
  _chatSubscription; 
  auxiliarList : any[];
  messages : any[] = [];
  number: number = 1;
  @ViewChild('content') private content: any;
  
  constructor(private route: ActivatedRoute, private router: Router, public db: AngularFireDatabase, private http : HttpClient) {
    this.route.queryParams.subscribe(params => {
        this.username = params.username;
    });
    this._chatSubscription = this.db.list('chat').valueChanges().subscribe(data => {
      this.messages = data;
      let index = 0;
      for(let prueba of this.messages){
        index++;
        if(!prueba.specialMessage && prueba.username != this.username){
          let mensaje =  this.traducion(prueba.message, index);
          console.log(mensaje);
          console.log(index);
          // prueba.message = mensaje;
        }
       
      }
     
      // console.log(this.messages);
    });    
  }
  traducion(texto: string, index : number){
    const headers = new HttpHeaders();
     
      let parametros : HttpParams;
    parametros = new HttpParams().append("ACTION","TRADUCIR.Traductor").append("source","ES").append("target","EN").append("q",texto);
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json; charset=utf-8'
        }),
        params: parametros
      };
      //(( new HttpParams().set('ACTION','TRADUCIR.Traductor').set("source",traduccion.idioma)
      // .set("target",traduccion.idiomaSecundario).set("q",traduccion.textoTraducir))) 192.168.1.132
      this.http.get<any[]>('http://localhost:8080/traductorApolo/Controller', httpOptions).subscribe(
        data => {
          console.log('POST Request is successful ', data);
          let JSONRecibido : any;
          let prueba : string;
          JSONRecibido = data;
          this.messages[index-1].message = this.parseHtmlEntities(JSONRecibido.data.translations[0].translatedText);
          console.log(this.messages);
          return JSONRecibido.data.translations[0].translatedText;
        },
        error => {
          console.log('Error', error);
          this.messages[index-1].message = texto;
        }
      );
  }
  parseHtmlEntities(str) {
    return str.replace(/&#([0-9]{1,3});/gi, function(match, numStr) {
        var num = parseInt(numStr, 10); // read num as normal number
        return String.fromCharCode(num);
    });
}

 /* Traduce el ultimo mensaje 
 constructor(private route: ActivatedRoute, private router: Router, public db: AngularFireDatabase) {
    this.route.queryParams.subscribe(params => {
        this.username = params.username;
    });
    this._chatSubscription = this.db.list('chat').valueChanges().subscribe(data => {
      // console.log(this.messages);
      this.auxiliarList = [];
      this.auxiliarList = this.messages;
      length: Number;
      length = data.length - this.auxiliarList.length;
      this.auxiliarList = data.slice(this.auxiliarList.length);
      console.log(this.auxiliarList);
      // Llamamos a traducir 
      data.splice(data.length,length);
      for(let messageAux of this.auxiliarList){
        console.log(messageAux);
        messageAux.message = messageAux.message.toUpperCase();
        //data.push(messageAux);
      }
      
      if(this.number == 1){
        this.messages = data;  
      } else {
        this.messages.push(this.auxiliarList);
        this.number -1 ;
      }
     
      // console.log(this.messages);
    });    
  }*/
  ngOnInit() {
    this.scrollToBottom();
  }
  
  sendMessage(){
    console.log("probando");
    this.db.list('/chat').push({
      username : this.username,
      message : this.message
    }).then( () => {

    }).catch( () => {

    });
    this.message = '';
    this.scrollToBottom();
  }

  scrollToBottom() { setTimeout( () => { 
    this.content.scrollToBottom();
   }) }
  ionViewDidEnter(){
    this.db.list('chat').push({
      specialMessage: true,
      message: `${this.username} has joined the room`
    })
  }

  ionViewWillLeave(){
    this._chatSubscription.unsuscribe();
    this.db.list('chat').push({
      specialMessage: true,
      message: `${this.username} has left the room`
    })
  }
}
