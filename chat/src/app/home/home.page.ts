import { Component } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  username: string = '';
  prueba : [];
  constructor(public navCtrl : NavController, private alertCtrl : AlertController, private router : Router) {}


  loginUser(){
    if(/^[a-zA-Z0-9]+$/.test(this.username)){
      let navigationExtras: NavigationExtras = {
        queryParams: {
          username: this.username
        }
      };
      this.router.navigate(['chat'], navigationExtras);
    } else {
      this.presentAlert('Error', 'Usuario Incorrecto');
    }
  }
  async presentAlert(title : string, msg : string,) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: msg,
      buttons: ['OK']
    });
    await alert.present();
  }
}
